package BubbleSortMusicPlayer;

import java.util.ArrayList;

public class Album {
    private ArrayList<Title> list = new ArrayList<Title>();

    public void addTitle(String title, String singer, int price) {
        Title item = new Title(title, singer, price);
        list.add(item);
    }

    public ArrayList<Title> getList() {
        return this.list;
    }

    public void setList(ArrayList<Title> arrayList) {
        this.list = arrayList;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Title title : list) {
            result.append(", ").append(((Title) title).getTitle());
        }

        return result.toString();
    }


    public static ArrayList<Title> BubbleSort(ArrayList<Title> arrayList)
    {
        Title tmp = arrayList.get(0);

        for (int i = 0; i < arrayList.size(); i++)
        {
            for (int j = i + 1; j < arrayList.size(); j++)
            {
                if (arrayList.get(i).getSongLength() > arrayList.get(j).getSongLength())
                {
                    tmp = arrayList.get(i);

                    arrayList.set(i, arrayList.get(j));
                    arrayList.set(j, tmp);
                }
            }
        }

        return arrayList;
    }

}
